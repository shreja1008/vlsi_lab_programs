`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:21:28 10/06/2020 
// Design Name: 
// Module Name:    eight_bit_parity 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eight_bit_parity(
    input [0:7]a,
    output even_par,
    output odd_par
    );
 assign even_par=^a;
 assign odd_par=~(^a);

endmodule
