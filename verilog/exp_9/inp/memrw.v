`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:49:25 10/19/2020 
// Design Name: 
// Module Name:    memrw 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module memrw(clk, data, out, rst);

input clk, rst;
input [7:0] data;
output [7:0] out;

reg [6:0] addr;



blk_mem_bram b1(clk, 1'b0, addr, data, out);





always @(posedge clk)

begin
	if (rst==1)
	addr = 6'b0;
	else
	addr = addr+1;
end



endmodule
