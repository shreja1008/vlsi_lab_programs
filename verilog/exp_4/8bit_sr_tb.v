`include "8bit_sr.v"

module top;
reg [7:0] parallel_in;
reg clk,s0,s1,shift_left_input,shift_right_input;
wire [7:0]out;

initial clk = 1'b1;
always #5 clk = ~clk;

eightbitsr shift_register (clk,s0,s1,parallel_in,shift_left_input,shift_right_input,out);

initial
begin
    parallel_in = 8'b10010101;
    shift_right_input=1;
    shift_left_input=0;
    #5 s0=1;s1=1;
    #5 s0=0;
    #5 s0=1;s1=0;
    #5 s0=0;
    #5 s0=1;
end

initial

begin
    $monitor($time,"  out = %b",out);
    $dumpfile("8bit_sr.vcd");
	$dumpvars;
    #200 $finish;
end

endmodule