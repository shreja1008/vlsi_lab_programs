module not_gate(f,e); 
input e; 
output f; 
assign f= ~e; 
endmodule
