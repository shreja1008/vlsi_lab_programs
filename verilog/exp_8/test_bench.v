`include arr_mult16_bit.v

module Test_bench;

  wire [31:0] p;
  reg [15:0] a;
  reg [15:0] b;

  ArrayMultiplier  am (p, a, b);
  initial $monitor("a=%d,x=%d,p=%d", a, b, p);


  initial
  begin
   a = 16'b0000000000110111;
   b = 16'b0000000000011111;
  end
endmodule