`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:29:26 10/13/2020 
// Design Name: 
// Module Name:    ArrayMultiplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module ArrayMultiplier(output [31:0]product, input [15:0]a, [15:0]b);

module half_adder(output Cnext, Sthis, input a1, a2, Slast, Cthis);

  wire t;
  and (t, a1, a2);

  xor (Sthis, t, Slast, Cthis);
  xor (t1, Slast, Cthis);
  and (t2, t, t1);
  and (t3, Cthis, Slast);
  or (Cnext, t2, t3);
  
endmodule

module Full_adder(output Cnext, Sthis, input a1, a2, Cthis);
  
  wire t1, t2, t3;
  xor (t1, a1, a2);
  and (t2, t1, Cthis);
  and (t3, a2, a1);
  or (Cnext, t2, t3);
  xor (Sthis, t1, Cthis);

endmodule

module ArrayMultiplier(product, a, b);
  
  output [31:0] product;
  input [15:0] a;
  input [15:0] b;
  
  wire c_partial[256:0] ;
  wire s_partial[256:0] ;
  
  // first line of the multiplier
  genvar i;
  generate
    for(i=0; i<16; i=i+1)
    begin
      //full adder fine tuned to act like a half adder
      half_adder c_first(c_partial[i],s_partial[i],b[0],a[i],1'b0,1'b0);
    end
  endgenerate
  
  
  // middle lines of the multiplier - except last column
  genvar j, k;
  generate
    for(k=0; k<15; k=k+1)
    begin
      for(j=0; j<15; j=j+1)
      begin
        half_adder c_middle(c_partial[16*(k+1)+j], s_partial[16*(k+1)+j],b[k+1], 
                            a[j], s_partial[16*(k+1)+j-15], c_partial[16*(k+1)+j-16]);
      end
    end
  endgenerate
  
  // middle lines of the multiplier - only last column
  genvar o;
  generate
    for(o=0; o<15; o=o+1)
    begin
      half_adder c_middle_last_col(c_partial[16*(o+1)+15], s_partial[16*(o+1)+15],
                                  b[o+1], a[15], 1'b0, c_partial[16*(o+1)-1]);
    end
  endgenerate
  
  // last line of the multiplier
  wire c_last_partial[15:0] ;
  wire s_last_partial[14:0] ;
  buf (c_last_partial[0], 0);
  
  genvar l;
  generate
    for(l=0; l<15; l=l+1)
    begin
      Full_adder c_last(c_last_partial[l+1], s_last_partial[l],c_partial[15*16+l], s_partial[15*16+l+1], c_last_partial[l]);
    end
  endgenerate
  
  
  // product bits from first and middle cells
  generate
    for(i=0; i<16; i=i+1)
    begin
      buf (product[i], s_partial[16*i]);
    end
  endgenerate
  
  // product bits from the last line of cells
  generate
    for(i=16; i<31; i=i+1)
    begin
      buf (product[i], s_last_partial[i-16]);
    end
  endgenerate
    
  // msb of product
  buf (product[31], c_last_partial[14]);

endmodule


