`include "fullAdder.v"

module top;
reg a,b,cin;
wire sum,ca;

fullAdder FA_0(a,b,cin,sum,ca);
initial
begin
    a=1'b0;
    #5 b=1'b0;
    #5 cin=1'b0;
    #5 a=1'b1;
    #5 b=1'b1;
    #5 cin=1'b1;
    #5 a=1'b0;

end

initial
begin
    $monitor($time,"a=%b;b=%b;cin=%b;sum=%b;ca=%b",a,b,cin,sum,ca);
end
endmodule