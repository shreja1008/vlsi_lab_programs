`include "eightfullAdder.v"

module top;
reg [7:0] a, b;
reg cin;
wire [7:0] sum;
wire c1,ca;

eightfullAdder F_0 (a[3:0], b[3:0], cin, sum[3:0], c1);
eightfullAdder F_1 (a[7:4], b[7:4], cin, sum[7:4], ca);

initial
begin
	a = 4'b0000;
	#5 b = 4'b0000;
	#5 cin = 1'b0;
	#5 a=4'b1111;
	#5 b=4'b1111;
	#5 cin=1'b1;
	#5 a=4'b1010;
end

initial
begin
	
	$monitor ($time, "a = %b; b = %b; cin = %b; sum = %b; ca = %b", a, b, cin, sum, ca);

endmodule
