`include "t2fullAdder.v"

module top;
reg [31:0] a,b;
reg cin;
wire [31:0]sum;
wire cout;

t2fullAdder F_0(a,b,cin,sum,cout);

initial //starts at exe time zero
begin
    cin=1'b0;
    #5 a=32'd10;
    #5 b=32'd10;
    #5 a=32'd200;
    #5 b=32'd23;
    #5 a=32'd144;
    #5 b=32'd5678;
    // #5 a=32'b11111111111111111111111111111111;
    // #5 b=32'b10;
    // #5 a=32'b100;
    
end

initial
begin
    
    $monitor($time,"a = %b  b = %b  cin = %b  sum = %b  cout = %b ",a,b,cin,sum,cout);
    $monitor($time,"a = %d  b = %d  cin = %d  sum = %d  cout = %d ",a,b,cin,sum,cout);
    $dumpfile("test.vcd");
	$dumpvars;
end

endmodule