`include "eightfullAdder.v"

module sixteenfullAdder(a,b,cin,sum,ca);
input [15:0] a,b;
input cin;

output [15:0] sum;
output c1,ca;

wire c1;

eightfullAdder F_0(a[7:0],b[7:0],cin,sum[7:0],c1);
eightfullAdder F_1(a[15:8],b[15:8],c1,sum[15:8],ca);

endmodule