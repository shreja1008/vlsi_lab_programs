`include "sixteenfullAdder.v"

module t2fullAdder(a,b,cin,sum,ca);
input [31:0] a,b;
input cin;

output [31:0] sum;
output c1,ca;

wire c1;

sixteenfullAdder F_0(a[15:0],b[15:0],cin,sum[15:0],c1);
sixteenfullAdder F_1(a[31:16],b[31:16],c1,sum[31:16],ca);

endmodule