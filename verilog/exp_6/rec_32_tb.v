`include "rec_32.v"
module top;
reg [31:0] a, b;
reg cin;
wire [31:0] sum;
wire cout;

b32_doub_rec moda(sum, cout,a, b,cin);
initial begin
    cin=1'b0;
    a = 32'b10000000010101100000000001010110;
     b = 32'b10000000010111010000000001011101;
    // a=32'b011111;
    // b = 32'b1011;
end
initial 
    $monitor("A:%b\nB:%b\nS:%b\ncout:%b\n", a, b, sum, cout);
    //$monitor("A:%d\nB:%d\nS:%d\n",a, b, sum);

endmodule