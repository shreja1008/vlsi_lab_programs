module adder32bit(a,b,cin,sum,cout);

    input [31:0] a,b;
    input cin;
    output reg [31:0] sum;
    output reg cout;
    reg [1:0] carry [32:0];

    integer i,j;

    always @(a or b or cin)
    begin

        // initialization
        carry[0][0]=cin+2; // set 1st carry as the given cin
        sum=a^b; // calculate intermediate sum

        // setting initial values of k,p,g in carry
        for(i=1;i<=32;i=i+1)
        begin
            case({a[i-1],b[i-1]})
                2'b00: carry[i]=2'b10; // set kill if both are 0
                2'b11: carry[i]=2'b11; // set generate if both are 1
                default: carry[i]=2'b0x; // set propagate if both are different
            endcase
        end

        // using recursive doubling to set carry values
        for(i=1;i<=16;i=i*2)
        begin
            for(j=i;j<=32;j=j+1)
            begin
                // carry[j]=carry[j]*carry[j-i]
                if(carry[j][1]==0) // if current carry is on propagate, get carry from j-i
                    carry[j]=carry[j-i];
            end
        end


        // finally xor intermediate sum with carry to get original sum
        for(i=0;i<32;i=i+1)
            sum[i]=sum[i]^carry[i][0];

        cout=carry[32][0]; // assign carry out
    end
    

endmodule