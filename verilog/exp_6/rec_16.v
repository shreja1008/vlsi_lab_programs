
`include "par.v"
module cla16bit(a, b, Sum, cout);
    input [15:0] a, b;
    output [15:0] Sum;
    output cout;
    wire [31:0] kpg, kpg1, kpg2, kpg3, kpg4;
    wire [15:0] xor_sum, carry;
    always@(a,b,)
    for(int i=0;i<16;i=i+1) 
    begin
         assign kpg[2*i] = a[i];
    end
    
    for(int i=0;i<16;i=i+1)
    begin
        int j=2*i+1;
         assign kpg[2*i+1] = b[i];
    end
   
    for(int i=0;i<16;i=i+1)
    begin
        monitor($time);
         assign xor_sum[i] = b[i] ^ a[i];
    end
    

    parallelprefix mod0(kpg[31:30], kpg[29:28], kpg1[31:30]);
    parallelprefix mod1(kpg[29:28], kpg[27:26], kpg1[29:28]);
    parallelprefix mod2(kpg[27:26], kpg[25:24], kpg1[27:26]);
    parallelprefix mod3(kpg[25:24], kpg[23:22], kpg1[25:24]);
    parallelprefix mod4(kpg[23:22], kpg[21:20], kpg1[23:22]);
    parallelprefix mod5(kpg[21:20], kpg[19:18], kpg1[21:20]);
    parallelprefix mod6(kpg[19:18], kpg[17:16], kpg1[19:18]);
    parallelprefix mod7(kpg[17:16], kpg[15:14], kpg1[17:16]);
    parallelprefix mod8(kpg[15:14], kpg[13:12], kpg1[15:14]);
    parallelprefix mod9(kpg[13:12], kpg[11:10], kpg1[13:12]);
    parallelprefix mod10(kpg[11:10], kpg[9:8], kpg1[11:10]);
    parallelprefix mod11(kpg[9:8], kpg[7:6], kpg1[9:8]);
    parallelprefix mod12(kpg[7:6], kpg[5:4], kpg1[7:6]);
    parallelprefix mod13(kpg[5:4], kpg[3:2], kpg1[5:4]);
    parallelprefix mod14(kpg[3:2], kpg[1:0], kpg1[3:2]);
    parallelprefix mod15(kpg[1:0], 2'b00, kpg1[1:0]);

    parallelprefix mod16(kpg1[31:30], kpg1[27:26], kpg2[31:30]);
    parallelprefix mod17(kpg1[29:28], kpg1[25:24], kpg2[29:28]);
    parallelprefix mod18(kpg1[27:26], kpg1[23:22], kpg2[27:26]);
    parallelprefix mod19(kpg1[25:24], kpg1[21:20], kpg2[25:24]);
    parallelprefix mod20(kpg1[23:22], kpg1[19:18], kpg2[23:22]);
    parallelprefix mod21(kpg1[21:20], kpg1[17:16], kpg2[21:20]);
    parallelprefix mod22(kpg1[19:18], kpg1[15:14], kpg2[19:18]);
    parallelprefix mod23(kpg1[17:16], kpg1[13:12], kpg2[17:16]);
    parallelprefix mod24(kpg1[15:14], kpg1[11:10], kpg2[15:14]);
    parallelprefix mod25(kpg1[13:12], kpg1[9:8], kpg2[13:12]);
    parallelprefix mod26(kpg1[11:10], kpg1[7:6], kpg2[11:10]);
    parallelprefix mod27(kpg1[9:8], kpg1[5:4], kpg2[9:8]);
    parallelprefix mod28(kpg1[7:6], kpg1[3:2], kpg2[7:6]);
    parallelprefix mod29(kpg1[5:4], kpg1[1:0], kpg2[5:4]);
    parallelprefix mod30(kpg1[3:2], 2'b00, kpg2[3:2]);
    parallelprefix mod31(kpg1[1:0], 2'b00, kpg2[1:0]);

    parallelprefix mod32(kpg2[31:30], kpg2[23:22], kpg3[31:30]);
    parallelprefix mod33(kpg2[29:28], kpg2[21:20], kpg3[29:28]);
    parallelprefix mod34(kpg2[27:26], kpg2[19:18], kpg3[27:26]);
    parallelprefix mod35(kpg2[25:24], kpg2[17:16], kpg3[25:24]);
    parallelprefix mod36(kpg2[23:22], kpg2[15:14], kpg3[23:22]);
    parallelprefix mod37(kpg2[21:20], kpg2[13:12], kpg3[21:20]);
    parallelprefix mod38(kpg2[19:18], kpg2[11:10], kpg3[19:18]);
    parallelprefix mod39(kpg2[17:16], kpg2[9:8], kpg3[17:16]);
    parallelprefix mod40(kpg2[15:14], kpg2[7:6], kpg3[15:14]);
    parallelprefix mod41(kpg2[13:12], kpg2[5:4], kpg3[13:12]);
    parallelprefix mod42(kpg2[11:10], kpg2[3:2], kpg3[11:10]);
    parallelprefix mod43(kpg2[9:8], kpg2[1:0], kpg3[9:8]);
    parallelprefix mod44(kpg2[7:6], 2'b00, kpg3[7:6]);
    parallelprefix mod45(kpg2[5:4], 2'b00, kpg3[5:4]);
    parallelprefix mod46(kpg2[3:2], 2'b00, kpg3[3:2]);
    parallelprefix mod47(kpg2[1:0], 2'b00, kpg3[1:0]);

    parallelprefix mod48(kpg3[31:30], kpg3[15:14], kpg4[31:30]);
    parallelprefix mod49(kpg3[29:28], kpg3[13:12], kpg4[29:28]);
    parallelprefix mod50(kpg3[27:26], kpg3[11:10], kpg4[27:26]);
    parallelprefix mod51(kpg3[25:24], kpg3[9:8], kpg4[25:24]);
    parallelprefix mod52(kpg3[23:22], kpg3[7:6], kpg4[23:22]);
    parallelprefix mod53(kpg3[21:20], kpg3[5:4], kpg4[21:20]);
    parallelprefix mod54(kpg3[19:18], kpg3[3:2], kpg4[19:18]);
    parallelprefix mod55(kpg3[17:16], kpg3[1:0], kpg4[17:16]);
    parallelprefix mod56(kpg3[15:14], 2'b00, kpg4[15:14]);
    parallelprefix mod57(kpg3[13:12], 2'b00, kpg4[13:12]);
    parallelprefix mod58(kpg3[11:10], 2'b00, kpg4[11:10]);
    parallelprefix mod59(kpg3[9:8], 2'b00, kpg4[9:8]);
    parallelprefix mod60(kpg3[7:6], 2'b00, kpg4[7:6]);
    parallelprefix mod61(kpg3[5:4], 2'b00, kpg4[5:4]);
    parallelprefix mod62(kpg3[3:2], 2'b00, kpg4[3:2]);
    parallelprefix mod63(kpg3[1:0], 2'b00, kpg4[1:0]);

    for(int i=0;i<16;i=i+1)
    begin
        int j=2*i+1;
    assign carry[i] = kpg4[j];
    end
    assign Sum[0] = xor_sum[0];
    for(int i=1;i<15;i=i+1)
    begin
    assign Sum[i] = xor_sum[i] ^ carry[i-1];
    end
    assign cout = carry[15];

endmodule
