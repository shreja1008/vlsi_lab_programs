def infix_postfix(infix_exp):
	stack=[]
	postfix_exp = []

	for i in infix_exp:
		if(i == '('):
			stack.append('(')
		elif(i == ')'):
			postfix_exp.append(stack[-1])
			stack.pop()
			stack.pop()
		elif(i == '+' or i == '!' or i == '.'):
			stack.append(i)
		else:
			postfix_exp.append(i)
	if stack:
		print("Insert brackets in appropriate places")
		exit()

	return postfix_exp

def gate_struct(exp):
	stack=[]
	simfile=[]
	out_no=0
	node_no=0
	for i in range(len(exp)):
		x=exp[i]
		print('The stacks '+str(i)+' is '+str(stack))
		if x=='.':
			a=stack.pop()
			b=stack.pop()
			simfile.append('p '+a+' vdd out'+str(out_no)+' 2 4\n')
			simfile.append('p '+b+' vdd out'+str(out_no)+' 2 4\n')
			simfile.append('n '+a+' gnd n'+str(node_no)+' 2 4\n')
			simfile.append('n '+b+' n'+str(node_no)+' out'+str(out_no)+' 2 4\n')

			simfile.append('p out'+str(out_no)+' vdd out'+str(out_no+1)+' 2 4\n')
			simfile.append('n out'+str(out_no)+' gnd out'+str(out_no+1)+' 2 4\n')
			stack.append('out'+str(out_no+1))
			out_no+=2
			node_no+=1

		elif(x=='+'):
			a=stack.pop()
			b=stack.pop()
	
			simfile.append('n '+a+' gnd out'+str(out_no)+' 2 4\n')
			simfile.append('n '+b+' gnd out'+str(out_no)+' 2 4\n')
			simfile.append('p '+a+' vdd n'+str(node_no)+' 2 4\n')
			simfile.append('p '+b+' n'+str(node_no)+' out'+str(out_no)+' 2 4\n')

			simfile.append('p out'+str(out_no)+' vdd out'+str(out_no+1)+' 2 4\n')
			simfile.append('n out'+str(out_no)+' gnd out'+str(out_no+1)+' 2 4\n')
			stack.append('out'+str(out_no+1))
	
			out_no+=2
			node_no+=1

		elif(x=='!'):
			a=stack.pop()
	
			simfile.append('p '+a+' vdd out'+str(out_no)+' 2 4\n')
			simfile.append('n '+a+' gnd out'+str(out_no)+' 2 4\n')
			stack.append('out'+str(out_no))
	
			out_no+=1

		else:
			stack.append(x)
	simfile.append('out'+str(out_no-1))

	return  simfile

if __name__ == '__main__':
	exp=input("Enter expression : ")
	filename=input("Enter filename : ")
	print('postfix expression is ')
	print(infix_postfix(exp))

	structure=gate_struct(infix_postfix(exp))

	output=structure.pop()
	f=open(filename,'w')
	f.writelines(structure)
	print('your output is ',output)
	print('sim file created successfully!')
	f.write('\n')
	f.close()